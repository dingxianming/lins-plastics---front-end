import request from '@/utils/request'

// 查询客户管理列表
export function listKehu (query) {
  return request({
    url: '/system/kehu/list',
    method: 'get',
    params: query
  })
}

// 查询客户管理详细
export function getKehu (id) {
  return request({
    url: '/system/kehu/' + id,
    method: 'get'
  })
}

// 新增客户管理
export function addKehu (data) {
  return request({
    url: '/system/kehu',
    method: 'post',
    data: data
  })
}

// 修改客户管理
export function updateKehu (data) {
  return request({
    url: '/system/kehu',
    method: 'put',
    data: data
  })
}

// 删除客户管理
export function delKehu (id) {
  return request({
    url: '/system/kehu/' + id,
    method: 'delete'
  })
}
