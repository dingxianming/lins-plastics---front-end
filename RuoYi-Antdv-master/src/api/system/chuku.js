import request from '@/utils/request'

// 查询出库管理列表
export function listChuku (query) {
  return request({
    url: '/system/chuku/list',
    method: 'get',
    params: query
  })
}

// 查询出库管理详细
export function getChuku (id) {
  return request({
    url: '/system/chuku/' + id,
    method: 'get'
  })
}

// 新增出库管理
export function addChuku (data) {
  return request({
    url: '/system/chuku',
    method: 'post',
    data: data
  })
}

// 修改出库管理
export function updateChuku (data) {
  return request({
    url: '/system/chuku',
    method: 'put',
    data: data
  })
}

// 删除出库管理
export function delChuku (id) {
  return request({
    url: '/system/chuku/' + id,
    method: 'delete'
  })
}

// 查询销售货物的销售额统计：
export function selectGoodsMoneyCount(query) {
  return request({
    url: '/system/chuku/selectGoodsMoneyCount',
    method: 'get',
    params: query
  })
}

// 查询销售货物的销售数量统计：
export function selectGoodsNumCount(query) {
  return request({
    url: '/system/chuku/selectGoodsNumCount',
    method: 'get',
    params: query
  })
}

// 查询支付状态统计
export function selectPayStatus(query) {
  return request({
    url: '/system/chuku/selectPayStatus',
    method: 'get',
    params: query
  })
}

// 查询销售额统计：
export function selectMoneyCount(query) {
  return request({
    url: '/system/chuku/selectMoneyCount',
    method: 'get',
    params: query
  })
}
